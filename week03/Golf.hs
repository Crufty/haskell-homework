module Golf where

-- Exercise 1: Hopscotch
-- First, a function to return the first item, followed by every nth item in a list
-- We do this by taking the first item, then use drop to discard the next n-1 items
-- and recurse with the remainder
firstAndEveryNth :: Int -> [a] -> [a]
firstAndEveryNth _ []		= []
firstAndEveryNth n (x:xs)	= x : firstAndEveryNth n (drop (n - 1) xs)

-- skips simply calls firstAndEveryNth with values of n from 1 to the list length
-- but we start at the nth item because firstAndEveryNth includes the first item which
-- we don't want (unless n == 1, but in that case we drop 0 items)
skips :: [a] -> [[a]]
skips xs = [ (firstAndEveryNth n (drop (n - 1) xs)) | n <- [1..(length xs)] ]

-- Two things to note:
-- 1. The above would possibly be clearer if wrote everyNth instead of firstAndEveryNth, but
-- I could not figure out as concise a solution that avoided using head or tail (which I'm told are teh evils)
-- 2. The pattern 'firstAndEveryNth n (drop (n - 1) xs' is duplicated in both functions. This tells me there is 
-- possibly some higher level function in the libraries I could use but I don't know enough to know how to apply them here.



-- Exercise 2: Local Maxima
-- Match strictly 3 elements followed by the rest of the list
-- Compare the middle of the 3 with it's siblings and if maxima return the middle, drop the first 2 and recurse on the remainder
-- if not maxima, drop the first element and recurse on the remainder
localMaxima :: [Integer] -> [Integer]
localMaxima (x:(y:(z:xs)))
	| (y > x) && (y > z)	= y : localMaxima (z : xs)
	| otherwise		= localMaxima (y : z : xs)
localMaxima _			= [] -- catchall for when less than 3 elements in the list


-- Exercise 3: Histogram

-- This function returns the number of times an int appears in a list of integers
howMany :: Int -> [Integer] -> Integer
howMany n xs = fromIntegral (length (filter (== (fromIntegral n)) xs))

-- This function takes a list of integers, and returns a list of counts of the digits 0-9 in order
digitFrequencies :: [Integer] -> [Integer]
digitFrequencies xs = [ howMany n xs | n <- [0..9] ]

-- This function takes an integer and a list of integers and returns a string with an asterisk in every place
-- that the given values appears in the list
starMatch :: Integer -> [Integer] -> String
starMatch n xs = map (\x -> if (x >= n) then '*' else ' ') xs

-- This function converts a list of integers into a list of strings representing a histogram
-- It should be called initialy with the maximum value of the y-axis as prints the top row first
-- and then decrements the value and recurses until it reaches the 0 value on the y-axis
-- It also adds the chrome and labels for the x-axis
histify :: Integer -> [Integer] -> [String]
histify n xs
	| (n > 0)	= starMatch n xs : histify (n - 1) xs
	| otherwise	= ["==========","0123456789"]

-- Calculate the frequency of digits in the list of integers and invoke histify to convert
-- that into a list of strings representing the ascii art of a histogram
histogram :: [Integer] -> String
histogram xs = unlines (histify (maximum (digitFrequencies xs)) (digitFrequencies xs))

-- Questions:
-- Is it possible to use a map and filter to perform the role of howMany and digitFrequencies? I tried and failed.
-- Is there a way to implement starMatch without the lambda?
-- In histogram, is (digitFrequencies xs) evaluated only once even if it appears twice?
