type Peg = String
type Move = (Peg, Peg)

move :: Peg -> Peg -> Move
move src dst = (src,dst)

-- First attempt at 3 peg hanoi
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi n src dst spare
  | n <= 0	= []
  | n == 1	= [move src dst]
  | n == 2	= move src spare : move src dst : [move spare dst]
  | otherwise	= hanoi (n - 1) src spare dst ++ move src dst : hanoi (n - 1) spare dst src


-- First attempt at 4 peg hanoi
hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4 n src dst sp1 sp2
  | n < 3	= hanoi n src dst sp1
  | n == 3	= move src sp1 : move src sp2 : move src dst : move sp2 dst : [move sp1 dst]
  | n == 4	= move src sp1 : move src dst : move src sp2 : move dst sp2 : move src dst : move sp2 src : move sp2 dst : move src dst : [move sp1 dst]
  | otherwise	= hanoi4 (n - 1) src sp1 dst sp2 ++ move src dst : hanoi4 (n - 1) sp1 dst src sp2

