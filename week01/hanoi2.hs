type Peg = String
type Move = (Peg, Peg)

move :: Peg -> Peg -> Move
move src dst = (src,dst)

-- Second attempt one line less than previous
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi n src spare dst
  | n <= 0	= []
  | n == 1	= [move src dst]
  | otherwise	= hanoi (n - 1) src dst spare ++ move src dst : hanoi (n - 1) spare src dst

