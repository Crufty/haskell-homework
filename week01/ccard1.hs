toDigitsRev :: Integer -> [Integer]
toDigitsRev n
  | n <= 0	= []
  | n < 10	= [n]
  | otherwise	= n `mod` 10 : toDigitsRev(n `div` 10)

toDigits :: Integer -> [Integer]
toDigits n	= reverse(toDigitsRev(n))

sumDigits :: [Integer] -> Integer
sumDigits []		= 0
sumDigits (x:xs)	= sum(toDigits(x)) + sumDigits(xs)

-- First attempt at this function, non-reversed is a bit clunky
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther []		= []
doubleEveryOther (x:[])		= [x]
doubleEveryOther (x:(y:[]))	= x * 2 : [y]
doubleEveryOther x		= doubleEveryOther(init(init(x))) ++ last(init(x)) * 2 : [last(x)]

validate :: Integer -> Bool
validate n	= (sumDigits(doubleEveryOther(toDigits(n))) `mod` 10) == 0
