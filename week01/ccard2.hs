toDigitsRev :: Integer -> [Integer]
toDigitsRev n
  | n <= 0	= []
  | n < 10	= [n]
  | otherwise	= n `mod` 10 : toDigitsRev(n `div` 10)

sumDigits :: [Integer] -> Integer
sumDigits []		= 0
sumDigits (x:xs)	= sum(toDigitsRev(x)) + sumDigits(xs)

-- Second attempt using cons only is tidier but works in reverse
doubleEveryOtherRev :: [Integer] -> [Integer]
doubleEveryOtherRev []		= []
doubleEveryOtherRev (x:[])	= [x]
doubleEveryOtherRev (x:(y:[]))	= x : [y * 2]
doubleEveryOtherRev (x:(y:zs))	= x : y * 2 : doubleEveryOtherRev(zs)

-- But that's ok because everything else works in reverse too!
-- This is my preferred solution because it is less code
validate:: Integer -> Bool
validate n	= (sumDigits(doubleEveryOtherRev(toDigitsRev(n))) `mod` 10) == 0


-- And a final solution using an unreversed version of the above for completeness
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther x = reverse(doubleEveryOtherRev(reverse(x)))

toDigits :: Integer -> [Integer]
toDigits n	= reverse(toDigitsRev(n))

validate':: Integer -> Bool
validate' n	= (sumDigits(doubleEveryOther(toDigits(n))) `mod` 10) == 0
