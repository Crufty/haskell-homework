type Peg = String
type Move = (Peg, Peg)

move :: Peg -> Peg -> Move
move src dst = (src,dst)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi n src spare dst
  | n <= 0	= []
  | n == 1	= [move src dst]
  | otherwise	= hanoi (n - 1) src dst spare ++ move src dst : hanoi (n - 1) spare src dst

-- An attempt to use the fewest moves possible
hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4 n src sp1 sp2 dst
  | n <= 2	= hanoi n src sp1 dst
  | n == 3	= move src sp1 : move src sp2 : move src dst : move sp2 dst : [move sp1 dst]
  | n == 4	= move src sp1 : move src dst : move src sp2 : move dst sp2 : move src dst : move sp2 src : move sp2 dst : move src dst : [move sp1 dst]
  | n == 5	= hanoi4 (n - 2) src sp1 dst sp2 ++ hanoi 2 src sp1 dst ++ hanoi4 (n - 2) sp2 src sp1 dst
  | otherwise	= hanoi4 (n - 3) src sp1 dst sp2 ++ hanoi 3 src sp1 dst ++ hanoi4 (n - 3) sp2 src sp1 dst

-- Try hanoi4 with the supplied k split point
hanoi4Try :: Integer -> Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4Try k n src sp1 sp2 dst
  | k >= n	= hanoi4 n src sp1 sp2 dst
  | otherwise	= hanoi4 k src sp1 dst sp2 ++ hanoi (n - k) src sp1 dst ++ hanoi4 k sp2 src sp1 dst

-- Try the entire range of split points
-- returns list of (k,NumMoves) pairs for every value of k
hanoi4TryAll :: Integer -> [(Integer,Int)]
hanoi4TryAll n = [ (k, length (hanoi4Try k n "a" "b" "c" "d")) | k <- [1..n]]

-- Find the pair (k,numMoves) from all the possible values of k that results in the least number of moves
-- There must be a better way to do this??? This is godawful
hanoi4Best :: Integer -> (Integer,Int)
hanoi4Best n = head [ (k,numMoves) | (k,numMoves) <- hanoi4TryAll n, numMoves == minimum [m | (k,m) <- hanoi4TryAll n] ]

-- Various other attempts to find optimisations
--  | n == 6	= hanoi4 (n - 3) src sp1 dst sp2 ++ hanoi 3 src sp1 dst ++ hanoi4 (n - 3) sp2 src sp1 dst
--  | n == 7	= hanoi4 (n - 3) src sp1 dst sp2 ++ hanoi 3 src sp1 dst ++ hanoi4 (n - 3) sp2 src sp1 dst
--  | n == 8	= hanoi4 (3) src sp1 dst sp2 ++ hanoi 3 src dst sp1 ++ hanoi4 3 sp2 src dst sp1 ++ hanoi 2 src sp2 dst ++ hanoi4 (6) sp1 src sp2 dst
--  | otherwise	= hanoi4 (n - 3) src sp1 dst sp2 ++ hanoi 3 src sp1 dst ++ hanoi4 (n - 3) sp2 src sp1 dst

