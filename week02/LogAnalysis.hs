{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where
import Log

-- Exercise 1
parseMessage :: String -> LogMessage
parseMessage s = case (words s) of
			("I":(d:m))	-> LogMessage Info (read d) (unwords m)
			("W":(d:m))	-> LogMessage Warning (read d) (unwords m)
			("E":(n:(d:m)))	-> LogMessage (Error (read n)) (read d) (unwords m)
			_		-> Unknown s

parseLines :: [String] -> [LogMessage]
parseLines []		= []
parseLines (x:xs)	= parseMessage x : parseLines xs

parse :: String -> [LogMessage]
parse s = parseLines (lines s)

-- Exercise 2
insert :: LogMessage -> MessageTree -> MessageTree
insert msg@(LogMessage _ _ _) Leaf	= Node Leaf msg Leaf
insert msg@(LogMessage _ msgDate _) (Node leftTree nodeMsg@(LogMessage _ nodeDate _) rightTree)
	| msgDate <= nodeDate	= Node (insert msg leftTree) nodeMsg rightTree
	| otherwise		= Node leftTree nodeMsg (insert msg rightTree)
insert _ tree			= tree -- Unknown and catchall

-- Exercise 3
build :: [LogMessage] -> MessageTree
build []	= Leaf
build (x:xs)	= insert x (build xs)

-- Excercise 4
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf					= []
inOrder (Node leftTree nodeMsg rightTree)	= (inOrder leftTree) ++ nodeMsg : (inOrder rightTree)

-- Exercise 5
whatWentWrong :: [LogMessage] -> [String]
whatWentWrong []	= []
whatWentWrong foo	= [ txt | (LogMessage (Error n) _ txt) <- (inOrder (build foo)), n > 50]

-- Exercise 6
-- Haven't actually read the book but I suspect it's The Hatter
